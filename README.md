# DeepVision Recognition Engine (DVRE)

## Clone

`$ git clone --recursive https://bitbucket.org/deepvisiondev/vf.demographics`

## Install

### Docker

#### ARM64

TODO

#### X86_64

`$ docker build --build-arg AWS_ACCESS_KEY_ID=<aws_access_key_id> --build-arg AWS_SECRET_ACCESS_KEY=<aws_secret_access_key> --build-arg AWS_DEFAULT_REGION=<aws_default_region> -t vf-demographics -f docker/Dockerfile_x86_64 .`

### Outside Docker
