# -*- coding: utf-8 -*-

import cv2

import json

from deepvision import BoundingBox

from multitarget_base_tracker import MultitargetBaseTracker


class MultitargetOCVTracker(MultitargetBaseTracker):
    def __init__(self, constructor):
        super().__init__()
        self._param = None
        self._constructor = constructor
        self._trackers = []

    @property
    def param(self):
        return self._param

    @param.setter
    def param(self, val):
        if not isinstance(val, dict):
            raise TypeError('param must be a dict')
        self._param = val

    def init(self, src, targets):
        if not isinstance(targets, (list, tuple)):
            raise TypeError('you must provide a list o BoundingBox instances')

        src = cv2.cvtColor(src, cv2.COLOR_RGB2BGR)

        for bbox in targets:
            tracker = self._constructor()

            if self._param is not None:
                fs = cv2.FileStorage()
                fs.open(json.dumps({"param": self._param}),
                        int(cv2.FILE_STORAGE_READ + cv2.FILE_STORAGE_MEMORY))
                tracker.read(fs.getFirstTopLevelNode())
                fs.release()

            tracker.init(src, bbox)
            self._targets.append(bbox)
            self._trackers.append(tracker)

    def update(self, src):
        src = cv2.cvtColor(src, cv2.COLOR_RGB2BGR)

        for idx in self.active_idxs():
            success, bbox = self._trackers[idx].update(src)
            if not success:
                self._targets[idx] = None
                self._trackers[idx] = None
            else:
                self._targets[idx] = BoundingBox(*bbox)

        return self._targets

    def reset_target(self, idx, src, bbox):
        self._targets[idx] = bbox
        self._trackers[idx].init(src, bbox)

    def mark_as_void(self, idxs):
        if not isinstance(idxs, (list, tuple)):
            raise TypeError("you must provide a list of int")

        for idx in idxs:
            if idx >= len(self._trackers):
                raise ValueError("index out of range")
            self._targets[idx] = None
            self._trackers[idx] = None

    def clean_void(self):
        to_keep = [i for i, t in enumerate(self._targets) if t is not None]
        self._targets = [self._targets[i] for i in to_keep]
        self._trackers = [self._trackers[i] for i in to_keep]


class MultitargetKCFTracker(MultitargetOCVTracker):
    def __init__(self):
        super().__init__(cv2.TrackerKCF_create)
        self.param = {
            "detect_thresh": 0.5,
            "sigma": 0.2,
            "lambda": 1e-4,
            "interp_factor": 0.075,
            "output_sigma_factor": 0.0625,
            "resize": 1,
            "max_patch_size": 6400,
            "split_coeff": 1,
            "wrap_kernel": 0,
            "desc_npca": 1,
            "desc_pca": 2,
            "compress_feature": 1,  # cv2.TRACKER_KCF_GRAY | cv2.TRACKER_KCF_CN
            "compressed_size": 2,
            "pca_learning_rate": 0.15
        }


class MultitargetCSRTTracker(MultitargetOCVTracker):
    def __init__(self):
        super().__init__(cv2.TrackerCSRT_create)
        self.param = {
            "padding": 3.0,
            "template_size": 200.0,
            "gsl_sigma": 1.0,
            "hog_orientations": 9.0,
            "num_hog_channels_used": 18,
            "hog_clip": 0.2,
            "use_hog": 1,
            "use_color_names": 1,
            "use_gray": 1,
            "use_rgb": 0,
            "window_function": "hann",
            "kaiser_alpha": 3.75,
            "cheb_attenuation": 45.0,
            "filter_lr": 0.02,
            "admm_iterations": 4,
            "number_of_scales": 33,
            "scale_sigma_factor": 0.25,
            "scale_model_max_area": 512.0,
            "scale_lr": 0.025,
            "scale_step": 1.0,
            "use_channel_weights": 1,
            "weights_lr": 0.02,
            "use_segmentation": 1,
            "histogram_bins": 16,
            "background_ratio": 2,
            "histogram_lr": 0.04,
            "psr_threshold": 0.035
        }


class MultitargetMedianFlowTracker(MultitargetOCVTracker):
    def __init__(self):
        super().__init__(cv2.TrackerMedianFlow_create)
        self.param = {
            "pointsInGrid": 10,
            "winSize": [3, 3],
            "maxLevel": 5,
            "termCriteria_maxCount": 20,
            "termCriteria_epsilon": 0.3,
            "winSizeNCC": [30, 30],
            "maxMedianLengthOfDisplacementDifference": 10.0
        }
