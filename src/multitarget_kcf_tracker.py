# -*- coding: utf-8 -*-

import cv2

from deepvision import BoundingBox

from multitarget_base_tracker import MultitargetBaseTracker


class MultitargetKCFTracker(MultitargetBaseTracker):
    def __init__(self):
        super().__init__()

        self._trackers = []

    def init(self, src, targets):
        if not isinstance(targets, (list, tuple)):
            raise ValueError('you must provide a list o BoundingBox instances')

        for bbox in targets:
            tracker = cv2.TrackerKCF_create()
            tracker.init(src, bbox)
            self._targets.append(bbox)
            self._trackers.append(tracker)

    def update(self, src):
        for idx in self.active_idxs():
            success, bbox = self._trackers[idx].update(src)
            if not success:
                self._targets[idx] = None
                self._trackers[idx] = None
            else:
                self._targets[idx] = BoundingBox(*bbox)

        return self._targets

    def reset_target(self, idx, src, bbox):
        self._targets[idx] = bbox
        self._trackers[idx].init(src, bbox)

    def mark_as_void(self, idxs):
        if not isinstance(idxs, (list, tuple)):
            raise TypeError("you must provide a list of int")

        for idx in idxs:
            if idx >= len(self._trackers):
                raise ValueError("index out of range")
            self._targets[idx] = None
            self._trackers[idx] = None

    def clean_void(self):
        to_keep = [i for i, t in enumerate(self._targets) if t is not None]
        self._targets = [self._targets[i] for i in to_keep]
        self._trackers = [self._trackers[i] for i in to_keep]
