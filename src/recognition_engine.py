# -*- coding: utf-8 -*-

import numpy as np

# from queue import Queue
# from threading import Thread

from deepvision import ROI
from deepvision.logging import get_logger
from deepvision.motion.base_tracklet import BaseTracklet
from deepvision.inference.base_estimator import BaseEstimator
from deepvision.inference.base_detector import BaseDetector

from multitarget_correlation_tracker import MultitargetCorrelationTracker
from multitarget_ocv_tracker import MultitargetKCFTracker

from face_tracklet import FaceTracklet

from haar_face_detector import HaarFaceDetector

from clock import Clock

import cv2


def run_detection(detector, input_queue, output_queue):
    while True:
        img = input_queue.get()
        input_queue.task_done()
        if img is None:
            break
        bboxes = detector.predict(img)
        output_queue.put(bboxes)
        output_queue.task_done()


class RecognitionEngine(object):
    def __init__(self, reader, detector, tracker, tracklet_type,
                 roi=None, detection_interval=1, association_thr=0.2):

        self._logger = get_logger(self.__class__.__name__)

        self._reader = reader

        # TODO: allow only BaseDetector
        if not isinstance(detector, (BaseDetector, BaseEstimator)):
            raise TypeError("detector must be valid detector")
        self._detector = detector

        # if not isinstance(tracker, MultitargetBaseTracker):
        #     raise TypeError("tracker must be multi-target")
        self._tracker = tracker

        self._tracklets = []
        self._tracklet_id_counter = 0
        self._tracklet_type = tracklet_type
        if not issubclass(self._tracklet_type, BaseTracklet):
            raise TypeError("t_tracklet must be a subclass of BaseTracklet")

        self._running = False

        self._clock = Clock()

        self.roi = roi

        self.detection_interval = detection_interval

        self.association_thr = association_thr

    def show(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

        if self._roi is not None:
            x1, y1 = self._roi.x, self._roi.y
            x2, y2 = self._roi.x + self._roi.width - 1, self._roi.y + self._roi.height - 1
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 128, 255), 3)

        for t in self._tracker.targets:
            if t is None:
                continue
            x1, y1 = int(t.x + 0.5), int(t.y + 0.5)
            x2, y2 = int(t.x + t.width + 0.5) - 1, int(t.y + t.height + 0.5) - 1
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 128), 3)

        cv2.imshow("{}".format(self.__class__), frame)

        ch = cv2.waitKey(1)
        if ch == 27 or ch in (ord('q'), ord('Q')):
            self._running = False

    @property
    def roi(self):
        return self._roi

    @roi.setter
    def roi(self, val):
        if not isinstance(val, (type(None), ROI)):
            raise TypeError("not a valid ROI object")
        self._roi = val
        self._logger.debug('ROI set to {}'.format(self._roi))

    @property
    def detection_interval(self, val):
        return self._detection_interval

    @detection_interval.setter
    def detection_interval(self, val):
        if float(val) < 0.:
            raise ValueError("not a valid detection interval")
        self._detection_interval = float(val)
        self._logger.debug('detection_interval set to {} sec'
                           ''.format(self._detection_interval))

    @property
    def association_thr(self, val):
        return self._association_thr

    @association_thr.setter
    def association_thr(self, val):
        if float(val) < 0.:
            raise ValueError("not a valid association threshold")
        self._association_thr = float(val)
        self._logger.debug('association_thr set to {} sec'
                           ''.format(self._association_thr))

    def _add_tracklet(self, img, bbox):
        # Get an id for the new tracklet
        id_ = self._tracklet_id_counter

        # Create a new empty face tracklet
        new_tracklet = self._tracklet_type(id_)
        new_tracklet.instances.append(self._new_instance(img, bbox))

        self._tracklets.append(new_tracklet)

        # Increment tracklet_id
        self._tracklet_id_counter += 1

        self._logger.debug('new tracklet (id={})'.format(id_))

    def _tt_sync(self):
        idxs = []
        for i, t in enumerate(self._tracker.targets):
            if (t is None) or \
               (self._roi is not None and not t.is_inside(self._roi)):
                idxs.append(i)

        self._tracker.mark_as_void(idxs)

        idxs = self._tracker.active_idxs()
        self._tracklets = [self._tracklets[i] for i in idxs]
        self._tracker.clean_void()

    def _tt_update(self, frame):
        if len(self._tracker.targets) != len(self._tracklets):
            raise RuntimeError('tracker targets ({}) / tracklets ({}) mismatch'
                               ''.format(len(self._tracker.targets),
                                         len(self._tracklets)))

        if len(self._tracker.targets) == 0:
            return

        self._logger.debug('updating {} target(s)'
                           ''.format(len(self._tracker.targets)))

        self._tracker.update(frame)

        for i, t in enumerate(self._tracker.targets):
            instance = self._new_instance(frame, t)
            self._tracklets[i].instances.append(instance)

        self._tt_sync()

    def _td_association(self, img, bboxes):
        if len(bboxes) == 0:
            return

        targets = self._tracker.targets

        if len(targets) == 0:
            idxs_new = list(range(len(bboxes)))
            idxs_ambiguous = []
            idxs_matches = []

        else:
            idxs_active = self._tracker.active_idxs()

            iou = np.atleast_2d([[b.overlap(targets[i]) for i in idxs_active]
                                 for b in bboxes])

            # assignments is a matrix of #bboxes x #targets
            assignments = (iou > self._association_thr).astype(np.uint8)
            row_assignments = assignments.sum(1)  # bboxes to targets
            col_assignments = assignments.sum(0)  # targets to bboxes

            # non matching bboxes (new targets)
            idxs_new = np.where(row_assignments == 0)[0].tolist()

            # bboxes assigned to more than one target
            idxs_ambiguous = np.where(row_assignments > 1)[0].tolist()

            # targets assigned to more than one bbox
            idxs_ambiguous_targets = np.where(col_assignments > 1)[0].tolist()
            if len(idxs_ambiguous_targets) > 0:
                # bboxes assigned to ambiguous targets
                idxs_ambiguous += sum([np.where(assignments[:, i])[0].tolist()
                                       for i in idxs_ambiguous_targets], [])
                idxs_ambiguous = list(set(idxs_ambiguous))

            # ambiguous matches are discarded

            # matches = all - {new U ambiguouous}
            idxs_matches = list(set(range(len(bboxes))) -
                                set(idxs_new + idxs_ambiguous))

            assignments = assignments.tolist()  # to avoid using np.where

            # idxs_matches = [(i, j)] = [from bbox i to target j]
            idxs_matches = [(i, assignments[i].index(1)) for i in idxs_matches]

            # i_target refers to idxs_active. Refer it back to the original
            # targets list
            idxs_matches = [(i, idxs_active[j]) for i, j in idxs_matches]

        self._logger.debug('new targets: {}, ambiguous: {}, matches: {}'
                           ''.format(len(idxs_new), len(idxs_ambiguous),
                                     len(idxs_matches)))

        for i_bbox, i_target in idxs_matches:
            self._tracker.reset_target(i_target, img, bboxes[i_bbox])
            instance = self._new_instance(img, bboxes[i_bbox])
            self._tracklets[i_target].instances[-1] = instance

        for i in idxs_new:  # + idxs_ambiguous:
            self._add_tracklet(img, bboxes[i])
        self._tracker.add_targets(img, [bboxes[i] for i in idxs_new])

        self._tt_sync()

    def _new_instance(self, img, bbox):
        if bbox is None or list(bbox) == []:
            return None
        return (bbox, bbox.crop(img),)

    def _process(self, frame):
        # process tracklets
        pass

    def _publish(self, frame):
        # send results to the frontend
        pass

    def run(self):
        self._running = True

        tracking_mod = 1

        # i_queue = Queue()
        # o_queue = Queue()
        # detection_thread = Thread(target=run_detection,
        #                           args=(self._detector, i_queue, o_queue),
        #                           daemon=True)
        # detection_thread.start()

        self._clock.reset()

        while self._running:
            # read frame
            err, frame = self._reader.read()
            if not err:
                break

            # update tracker + tracklets
            self._tt_update(frame)

            # run detector
            if self._clock.elapsed() >= self._detection_interval:
                self._clock.reset()
                bboxes = self._detector.predict(frame)
                self._logger.debug('{} detections'.format(len(bboxes)))
                if len(bboxes) > 0:
                    self._td_association(frame, [b for b, _, _ in bboxes])

            self.show(frame)

            # # run detector
            # if self._clock.elapsed() >= self._detection_interval:
            #     i_queue.put(frame)
            #     while len(o_queue) == 0:
            #         err, frame = self._reader.read()
            #         if not err:
            #             break
            #         self._tracker.update(frame)
            #     bboxes = o_queue.get()
            #     self._td_association(frame, bboxes)

            # update traker on frames queued during detection
            i = 0
            while self._reader.queue_size() > 0:
                err, frame = self._reader.read()
                if not err:
                    break

                if i % tracking_mod == 0:
                    continue
                i += 1

                self._tt_update(frame)

                self.show(frame)

            # process tracklets (using last tracked frame)
            self._process(frame)

            # publish results (using last tracked frame)
            self._publish(frame)

            # update traker on frames queued during process + publish
            while self._reader.queue_size() > 0:
                err, frame = self._reader.read()
                if not err:
                    break
                self._tt_update(frame)

                if i % tracking_mod == 0:
                    continue
                i += 1

                self.show(frame)


import sys
from deepvision import BoundingBox
from deepvision.improc import resize
from deepvision.io import WebCamReader, VideoReader, ProxyReader
from deepvision.inference import YoloDetector
from deepvision.motion import MultitargetMedianFlowTracker

# class MultitargetMedianFlowTracker_(YoloDetector):
#     def __init__(self, min_tracking_quality=1., *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._min_tracking_quality = int(min_tracking_quality)

#     def predict(self, x, **kwargs):
#         preds = super().predict(x, **kwargs)
#         return [bbox for bbox in preds if min(bbox[2:]) > self._min_size]


class ProxyshReader(VideoReader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._size = (1024, 576)

    def read(self):
        retval, frame = super().read()
        if retval:
            frame = resize(frame, self._size, 'bilinear')
        return retval, frame


class ProxyMultitargetTracker(object):
    def __init__(self, tracker, scale=0.33):
        self._tracker = tracker
        self._scale = float(scale)
        self._targets = tracker._targets

    @staticmethod
    def scale(src, scale):
        """
        Scales the input given a scale factor in (0, 1].
        The input can be an image or a bounding box.
        """
        if src is None or np.isclose(scale, 1.):
            return src

        if isinstance(src, BoundingBox):
            new_x, new_y, new_w, new_h = (
                scale * src.x,
                scale * src.y,
                scale * src.width,
                scale * src.height
            )
            dst = BoundingBox(new_x, new_y, new_w, new_h)

        elif isinstance(src, np.ndarray):
            h, w = src.shape[:2]
            new_h, new_w = (int(scale * h + 0.5), int(scale * w + 0.5))
            dst = resize(src, (new_w, new_h), "bilinear")

        else:
            raise RuntimeError("wrong type")

        return dst

    def init(self, src, targets=[]):
        src = self.scale(src, self._scale)
        targets = [self.scale(t, self._scale) for t in targets]
        self._tracker.init(src, targets)

    def update(self, src):
        src = cv2.cvtColor(src, cv2.COLOR_RGB2GRAY)
        src = self.scale(src, self._scale)
        return self._tracker.update(src)

    def add_targets(self, src, targets):
        src = self.scale(src, self._scale)
        targets = [self.scale(t, self._scale) for t in targets]
        return self._tracker.add_targets(src, targets)

    def mark_as_void(self, idxs):
        return self._tracker.mark_as_void(idxs)

    def active_idxs(self):
        return self._tracker.active_idxs()

    def clean_void(self):
        self._tracker.clean_void()
        self._targets = self._tracker.targets

    def reset_target(self, idx, src, target):
        src = self.scale(src, self._scale)
        target = self.scale(target, self._scale)
        self._tracker.reset_target(idx, src, target)

    @property
    def targets(self):
        return [self.scale(t, 1./self._scale) for t in self._targets]


def main():
    if len(sys.argv) < 2:
        print('Usage: python {} <video_path>'.format(sys.argv[0]))
        sys.exit(0)

    reader = WebCamReader(0)
    #reader = VideoReader(sys.argv[1], use_queue=True, maxsize=100, force_fps=True)
    #reader = ProxyshReader(sys.argv[1], use_queue=True, maxsize=100, force_fps=True)

    # tracker = MultitargetCorrelationTracker(quality_thr=5.)
    tracker = MultitargetKCFTracker()
    # tracker = ProxyMultitargetTracker(
    #     MultitargetCorrelationTracker(), 0.33)

    #detector = YoloDetector(model='face')
    detector = HaarFaceDetector()

    roi = None
    # roi = ROI(250, 50, 1024-250, 576-50)
    engine = RecognitionEngine(reader, detector, tracker, FaceTracklet, roi=roi)
    engine.run()


if __name__ == '__main__':
    main()

    # decoding:
    # - no much improvement for a single stream
    # - deepstream
    #   + multichannel per docker image simple
    #   + existing python bindigs to gstreamer

    # pipeline:
    # - 1/4 detection
    #   + tensorflow-tensorrt
    #   + tensorrt (SSD 2x, Yolo 3x)
    #
    # - ?/? feature extraction (dlib)
    #
    # - 1/4 x 2 correlation tracking
    #   + multithread
    #   + Opencv -> gpu
    #
    # - 1/4 recognition (2k subjets ~20k images on the dataset -> 100ms)
    #   + numpy -> pycuda/cupy
