# -*- coding: utf-8 -*-

import cv2

import json

from deepvision import BoundingBox

from deepvision.motion.base_tracker import BaseTracker


class OCVTracker(BaseTracker):

    def __init__(self, constructor, param):
        super().__init__()

        self._tracker = constructor()

        if param is not None:
            fs = cv2.FileStorage()
            fs.open(json.dumps({"param": param}),
                    int(cv2.FILE_STORAGE_READ + cv2.FILE_STORAGE_MEMORY))
            self._tracker.read(fs.getFirstTopLevelNode())
            fs.release()

    def init(self, src, target):
        src = cv2.cvtColor(src, cv2.COLOR_RGB2BGR)
        self._target = BoundingBox(*target)
        self._tracker.init(src, self._target)

    def update(self, src, target=None):
        src = cv2.cvtColor(src, cv2.COLOR_RGB2BGR)

        success, bbox = self._tracker.update(src)
        if not success:
            self._target = None
        else:
            self._target = BoundingBox(*bbox)

        return 1.0  # quality ?


class KCFTracker(OCVTracker):
    def __init__(self):
        param = {
            "detect_thresh": 0.5,
            "sigma": 0.2,
            "lambda": 1e-4,
            "interp_factor": 0.075,
            "output_sigma_factor": 0.0625,
            "resize": 1,
            "max_patch_size": 6400,
            "split_coeff": 1,
            "wrap_kernel": 0,
            "desc_npca": 1,
            "desc_pca": 2,
            "compress_feature": 1,  # cv2.TRACKER_KCF_GRAY | cv2.TRACKER_KCF_CN
            "compressed_size": 2,
            "pca_learning_rate": 0.15
        }
        super().__init__(cv2.TrackerKCF_create, param)


def main():
    from deepvision.io import WebCamReader
    reader = WebCamReader(0)

    tracker = None

    while True:
        success, frame = reader.read()
        if not success:
            break

        if tracker is None:
            tracker = KCFTracker()
            tracker.init(frame, BoundingBox(249, 270, 100, 150))
        else:
            tracker.update(frame)

        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

        x, y, w, h = tracker.target
        cv2.rectangle(frame, (int(x), int(y)), (int(x+w), int(y+h)),
                      (0, 0, 255), 2)
        cv2.imshow("test", frame)

        ch = cv2.waitKey(1)
        if ch == 27 or ch in (ord('q'), ord('Q')):
            break


if __name__ == '__main__':
    main()
