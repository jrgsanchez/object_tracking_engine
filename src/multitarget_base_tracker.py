# -*- coding: utf-8 -*-

from deepvision.logging import get_logger


class MultitargetBaseTracker(object):
    """
    TODO
    """
    def __init__(self):
        self.logger = get_logger(self.__module__ + '.' +
                                 self.__class__.__name__)
        self._targets = []

    @property
    def targets(self):
        return self._targets

    def init(self, src, targets=[]):
        raise NotImplementedError('must implement init')

    def update(self, src):
        raise NotImplementedError('must implement update')

    def add_targets(self, src, targets):
        return self.init(src, targets)

    def reset_target(self, idx, src, target):
        self._targets[idx] = target

    def mark_as_void(self, idxs):
        if not isinstance(idxs, (list, tuple)):
            raise TypeError("you must provide a list of int")

        for idx in idxs:
            if idx >= len(self._trackers):
                raise ValueError("index out of range")
            self._targets[idx] = None

    def clean_void(self):
        self._targets = [t for t in self._targets if t is not None]

    def active_idxs(self):
        return [i for i, t in enumerate(self._targets) if t is not None]
