# -*- coding: utf-8 -*-

import os

import cv2

from deepvision import BoundingBox
from deepvision.inference.base_detector import BaseDetector


class HaarFaceDetector(BaseDetector):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._model = cv2.CascadeClassifier()
        model_file = "{}/haarcascades/haarcascade_frontalface_alt.xml"
        ret = self._model.load(model_file.format(
            os.path.join(os.environ["HOME"], ".local/share/OpenCV")))
        if not ret:
            ret = self._model.load(model_file.format("/usr/share/OpenCV"))
        if not ret:
            ret = self._model.load(model_file.format("/usr/local/share/OpenCV"))
        if not ret:
            raise RuntimeError("can\'t find Haar model file")

        self._param = {
            'scaleFactor': 1.1,
            'minNeighbors': 5,
            # 'minSize': (24, 24),
            # 'maxSize': (128, 128)
        }

    def _predict(self, img):
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        faces = self._model.detectMultiScale(img, **self._param)

        faces = [(BoundingBox(x, y, w, h), 0, 1.0)
                 for x, y, w, h in faces]

        return faces
