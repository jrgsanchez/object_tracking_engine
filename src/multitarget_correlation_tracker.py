# -*- coding: utf-8 -*-

from dlib import correlation_tracker, rectangle

from deepvision import BoundingBox

from multitarget_base_tracker import MultitargetBaseTracker


class MultitargetCorrelationTracker(MultitargetBaseTracker):
    def __init__(self, quality_thr=5.):
        super().__init__()

        self.quality_thr = quality_thr

        self._trackers = []

    def _bbox2rect(self, bbox):
        x1 = int(bbox.x)
        y1 = int(bbox.y)
        x2 = int(bbox.x + bbox.width + 0.5) - 1
        y2 = int(bbox.y + bbox.height + 0.5) - 1
        return rectangle(x1, y1, x2, y2)

    def _rect2bbox(self, rect):
        x = rect.left()
        y = rect.top()
        w = rect.right() - rect.left() + 1
        h = rect.bottom() - rect.top() + 1
        return BoundingBox(x, y, w, h)

    @property
    def quality_thr(self):
        return self._quality_thr

    @quality_thr.setter
    def quality_thr(self, val):
        self._quality_thr = float(val)

    def init(self, src, targets):
        if not isinstance(targets, (list, tuple)):
            raise ValueError('you must provide a list o BoundingBox instances')

        for bbox in targets:
            tracker = correlation_tracker()
            tracker.start_track(src, self._bbox2rect(bbox))
            self._targets.append(bbox)
            self._trackers.append(tracker)

    def update(self, src):
        for idx in self.active_idxs():
            quality = self._trackers[idx].update(src)

            if quality < self._quality_thr:
                self._targets[idx] = None
                self._trackers[idx] = None
            else:
                self._targets[idx] = self._rect2bbox(
                    self._trackers[idx].get_position()
                )

        return self._targets

    def reset_target(self, idx, src, bbox):
        self._targets[idx] = bbox
        rect = self._bbox2rect(bbox)
        self._trackers[idx].start_track(src, rect)

    def mark_as_void(self, idxs):
        if not isinstance(idxs, (list, tuple)):
            raise TypeError("you must provide a list of int")

        for idx in idxs:
            if idx >= len(self._trackers):
                raise ValueError("index out of range")
            self._targets[idx] = None
            self._trackers[idx] = None

    def clean_void(self):
        to_keep = [i for i, t in enumerate(self._targets) if t is not None]
        self._targets = [self._targets[i] for i in to_keep]
        self._trackers = [self._trackers[i] for i in to_keep]
