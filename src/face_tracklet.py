# coding: utf-8

from deepvision.motion.base_tracklet import BaseTracklet


class FaceTracklet(BaseTracklet):
    def __init__(self, _id):
        super(FaceTracklet, self).__init__(_id)
