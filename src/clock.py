# -*- coding: utf-8 -*-

from datetime import datetime as dt

import time

_time = None


def tic():
    global _time
    _time = time.time()


def toc():
    global _time
    return time.time() - _time


class Clock(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self._t = dt.now()

    def elapsed(self):
        delta = dt.now() - self._t
        return delta.seconds + 1e-6 * delta.microseconds
